# Lab 01 - August 2nd 2018

## Added

* Git Repo setup 
* Swin admin account added to repo
* README file synced and updated
* Lab 01 Folder + Task Files synced to repo

## Issues
* Minor 'first-time-using-bitbucket' problems
* Learned how to use MD syntax