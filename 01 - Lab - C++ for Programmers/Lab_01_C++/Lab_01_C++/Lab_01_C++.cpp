#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <random>
#include <string>
#include <sstream>

using namespace std;

void FunctionOne(int x,  int y) {
	cout << x << ","; 
	cout << y;
	cout << endl;
}

int FunctionTwo(int x) {
	return x*x;
}

void FunctionFour() {
	for (int i = 0; i < 20; i++)
	{
		if (i % 2 != 0) {
			cout << i << " ";
		}
	}
}

//Random numbers array
void FunctionFive() {
	int arr[5]; 

	for (int i = 0; i < 5; i++)
	{
		arr[i] = rand();
		cout << arr[i] << endl;
	}
}

//String splitter
void FunctionSix(const string &str) {
	stringstream ss(str);
	string word;
	char delim = ' ';
	while (getline(ss, word, delim)) {
		cout << word << endl;
	}
}

//Task 7 - Custom Class
class Human {
	int age;
public:
	string name;
	void PrintNameAge();
	Human(string name, int age);
};
//Human constructor
Human::Human(string n, int a) {
	name = n;
	age = a;
}

void Human::PrintNameAge() {
	cout << "My name is " << name << " and I am " << age << " years old." << endl;
}


//Includes sporadic endls for console readability 
int main()
{
	//Task 1
	FunctionOne(5,10);

	//Task 2
	cout << FunctionTwo(12) << endl;

	//Task 3.
	int variable = 10;
	int *pointer;
	pointer = &variable;
	cout << "Reading variable from pointer: " << *pointer << endl;
	//Update value @pointer via dereference
	*pointer = 15;
	cout << "Reading variable from pointer: " << *pointer << endl;

	//Task 4
	FunctionFour();
	cout << endl;

	//Task 5
	FunctionFive();
	cout << endl;

	//Task 6
	FunctionSix("This is a string with spaces!");

	//Task 7
	Human tyler("Tyler", 25);
	tyler.PrintNameAge();

	cout << endl;
	return 0;
}

