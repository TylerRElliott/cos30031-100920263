//Lab 24 + 26

#include "stdafx.h"
#include <iostream>
#include <SDL.h>

using namespace std;

//Yoinked from StackOverflow.
typedef int32_t s32;
void DrawCircle(SDL_Renderer *Renderer, s32 _x, s32 _y, s32 radius)
{
	s32 x = radius - 1;
	s32 y = 0;
	s32 tx = 1;
	s32 ty = 1;
	s32 err = tx - (radius << 1); // shifting bits left by 1 effectively
								  // doubles the value. == tx - diameter
	while (x >= y)
	{
		//  Each of the following renders an octant of the circle
		SDL_RenderDrawPoint(Renderer, _x + x, _y - y);
		SDL_RenderDrawPoint(Renderer, _x + x, _y + y);
		SDL_RenderDrawPoint(Renderer, _x - x, _y - y);
		SDL_RenderDrawPoint(Renderer, _x - x, _y + y);
		SDL_RenderDrawPoint(Renderer, _x + y, _y - x);
		SDL_RenderDrawPoint(Renderer, _x + y, _y + x);
		SDL_RenderDrawPoint(Renderer, _x - y, _y - x);
		SDL_RenderDrawPoint(Renderer, _x - y, _y + x);

		if (err <= 0)
		{
			y++;
			err += ty;
			ty += 2;
		}
		if (err > 0)
		{
			x--;
			tx += 2;
			err += tx - (radius << 1);
		}
	}
}

//Key press surfaces constants
enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL
};

bool bgActive = true;
SDL_Surface* gKeyPressSurfaces[KEY_PRESS_SURFACE_TOTAL];
SDL_Surface* gCurrentSurface = NULL;
SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Surface* bg = NULL;
SDL_Renderer* renderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
SDL_Rect gSpriteClips[3];

SDL_Surface* loadSurface(std::string path)
{
	//Load image at specified path
	SDL_Surface* loadedSurface = SDL_LoadBMP(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
	}

	return loadedSurface;
}


bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load default surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadSurface("bg.bmp");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL)
	{
		printf("Failed to load default image!\n");
		success = false;
	}

	//Load up surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] = loadSurface("bgOff.bmp");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] == NULL)
	{
		printf("Failed to load up image!\n");
		success = false;
	}

	//Load down surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] = loadSurface("1.bmp");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] == NULL)
	{
		printf("Failed to load down image!\n");
		success = false;
	}

	//Load left surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] = loadSurface("1.bmp");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] == NULL)
	{
		printf("Failed to load left image!\n");
		success = false;
	}
	return success;
}



bool init() {

	bool success = true;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		gWindow = SDL_CreateWindow("Lab 24", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			gScreenSurface = SDL_GetWindowSurface(gWindow);
		}
	}

	return success;
}

void toggleBg() {
	if (bgActive) {
		cout << "True" << endl;
		gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
		bgActive = false;
	}
	else {
		cout << "False" << endl;
		gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_UP];
		bgActive = true;
	}
	cout << gCurrentSurface << endl;
}

//Function required for circle-circle collision checking
double distanceSquared(int x1, int y1, int x2, int y2)
{
	int deltaX = x2 - x1;
	int deltaY = y2 - y1;
	return deltaX*deltaX + deltaY*deltaY;
}

int main(int argc, char* argv[]) {

	bool quit = false;
	SDL_Event e;

	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
		return 1;
	}
	else {
		loadMedia();
	}

	//Rects + Circles
	SDL_Rect r;
	r.x = 300;
	r.y = 25;
	r.w = 50;
	r.h = 50;
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
	SDL_Rect r2;
	r2.x = 400;
	r2.y = 1;
	r2.w = 50;
	r2.h = 50;
	
	int circ1x = 300;
	int circ1y = 300;
	int circ1rad = 25;
	int circ2x = 400;
	int circ2y = 300;
	int circ2rad = 45;
	
	//Game loop
	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYDOWN)
			{
				//Select surfaces based on key press
				switch (e.key.keysym.sym)
				{
				case SDLK_KP_0:
					toggleBg();
					break;

				case SDLK_KP_1:
					gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN];
					break;

				case SDLK_KP_2:
					gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT];
					break;

				case SDLK_KP_3:
					gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT];
					break;
				}
			}
		}
		//Clear previous render
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);
		SDL_BlitSurface(gCurrentSurface, NULL, gScreenSurface, NULL);
		SDL_UpdateWindowSurface(gWindow);
		
		
		/*Collisions Lab
		
		
		//Clear previous render
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		//Update shape pos
		if (r.x > 640) {
			r.x = 0;
		}
		if (r2.x < 0) {
			r2.x = 640;
		}
		if (circ1x > 640) {
			circ1x = 0;
		}
		r.x++;
		r2.x--;
		circ1x+=1;

		//Check for rect collisions
		if (((r.x + 50) > r2.x) && (r.x) < (r2.x+50) ) {
			SDL_SetRenderDrawColor(renderer, 30, 144, 0, 255);
		}
		else {
			SDL_SetRenderDrawColor(renderer, 30, 144, 255, 255);
		}
		//Render Rect 1
		SDL_RenderDrawRect(renderer, &r);
		SDL_RenderFillRect(renderer, &r);

		//Render Rect 2
		
		SDL_SetRenderDrawColor(renderer, 30, 255, 255, 255);
		SDL_RenderDrawRect(renderer, &r2);
		SDL_RenderFillRect(renderer, &r2);

		//Check for circle collisions
		int totalRadSqr = circ1rad + circ2rad;
		totalRadSqr = totalRadSqr*totalRadSqr;

		if (distanceSquared(circ1x, circ1y, circ2x, circ2y) < (totalRadSqr)) {
			//Collision
			SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
		}
		else {
			//No Collision
			SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255);
		}
		//Render Circle 1
		DrawCircle(renderer, circ1x, circ1y, circ1rad);

		//Render Circle 2
		DrawCircle(renderer, circ2x, circ2y, circ2rad);

		SDL_RenderPresent(renderer);
		SDL_Delay(5);
		*/
	}
	return 0;
}