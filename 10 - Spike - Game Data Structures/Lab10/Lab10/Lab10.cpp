//TYLER R ELLIOTT - 10092063

#include "stdafx.h"
#include <iostream>
#include "Item.h";
#include "Inventory.h";

using namespace std;

int main()
{	
	Inventory backpack;

	Weapon axe(10);
	axe._name = "Axe";
	axe._itemID = 1;

	Weapon pistol(500);
	pistol._name = "Pistol";
	pistol._itemID = 2;

	backpack.AddItem(axe);
	backpack.AddItem(pistol);
	backpack.PrintItems();
	cout << endl;
	cout << "Deleting the pistol..." << endl;
	cout << endl;
	backpack.RemoveItem("Pistol");

	backpack.PrintItems();
    return 0;
}

