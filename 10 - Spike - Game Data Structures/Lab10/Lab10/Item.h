#pragma once
#include <string>

using namespace std;

//All inventory items inherit from this base class.
class Item {
public:
	string _name;
	int _itemID;
};

class Weapon : public Item {
public:
	int damageDealt;
	Weapon(int dmg);
};
Weapon::Weapon(int d) {
	damageDealt = d;
}

class Armor : public Item {
public:
	int resistance;
};