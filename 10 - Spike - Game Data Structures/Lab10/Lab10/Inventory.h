#pragma once
#include <map>
#include <iostream>
#include "Item.h"

using namespace std;

class Inventory {
public:
	//A map of inventory items, stored with a string (name) as key.
	map<string, Item> inv;

	void AddItem(Item i);
	void RemoveItem(string name);

	void PrintItems();
};
void Inventory::AddItem(Item i) {
	inv.insert(pair<string, Item>(i._name, i));
}
void Inventory::RemoveItem(string name) {
	inv.erase(name);
}
void Inventory::PrintItems() {
	cout << "--INVENTORY CONTENTS--" << endl;
	for (auto& kv : inv) {
		auto x = inv[kv.first];
		cout << "Item: " << x._name << " ID: " << x._itemID << endl;
	}
}
