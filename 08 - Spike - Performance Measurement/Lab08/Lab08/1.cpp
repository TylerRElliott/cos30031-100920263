#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <chrono>
#include <random>
#include <fstream>
#include <string>

using namespace std;
using namespace std::chrono;


// - count char using slow repeated string::find_first_of
const double count_char_using_find_first_of(string s, char delim)
{
	auto start = steady_clock::now();
	int count = 0;
	auto pos = s.find_first_of(delim);
	while ((pos = s.find_first_of(delim, pos)) != string::npos){
		count++;
		pos++;
	}
	auto end = steady_clock::now();
	duration<double> diff = end - start;
	//cout << "Time to complete (SLOW): " << diff.count() << endl;

	return diff.count();
}

// - count char using fast std::count
const double count_char_using_count(string s, char delim)
{
	auto start = steady_clock::now();
	int c = count(s.begin(), s.end(), delim);
	auto end = steady_clock::now();
	duration<double> diff = end - start;
	//cout << "Time to complete (FAST): " << diff.count() << endl;
	return diff.count();
}


void exponential_rampup_test()
{
	cout << " << Exponential Ramp-up Test >> " << endl;
	int total;
	// ull (suffix) == "unsigned long long" in c
	for (auto size = 1ull; size < 1000000000ull; size *= 100)
	{
		// 1. get start time
		auto start = steady_clock::now();
		// 2. do some work (create, fill, find sum)
		vector<int> v(size, 42);
		total = accumulate(v.begin(), v.end(), 0u);
		// 3. show duration time
		auto end = steady_clock::now();
		duration<double> diff = end - start;
		cout << " - size: " << size << ", time: " << diff.count() << " s";
		cout << ", time/int: " << diff.count() / size << "s/int" << endl;

		// TIP: time in nanoseconds? Cast result of chrono::duration.count() ...
		// auto _dur = duration_cast<nanoseconds>( end - start ).count();
		// cout << _dur << endl;
	}
	cout << "done." << endl;
}

void linear_rampup_test()
{
	cout << " << Linear Ramp-up Test >> " << endl;
	int total;
	for (auto size = 1; size <= 5; size += 1)
	{
		int vec_size = size * 10000;
		// 1. get start time
		auto start = steady_clock::now();
		// 2. do some work (create, fill, find sum)
		vector<int> v(vec_size, 42);
		// std::accumulate (from <numeric>) collects from begin, to end
		// - in this case (default) it is the sum total of all the values in v
		total = accumulate(v.begin(), v.end(), 0u);
		// 3. show duration time
		auto end = steady_clock::now();
		duration<double> diff = end - start;
		cout << " - size: " << vec_size << ", time: " << diff.count() << " s";
		cout << ", time/int: " << diff.count() / vec_size << "s/int" << endl;
	}
	cout << "done." << endl;
}

string generateRandomString() {
	std::random_device rd;
	std::mt19937 eng(rd());
	
	std::uniform_int_distribution<>asciiDistr(65, 90);
	std::uniform_int_distribution<>lengthDistr(1, 50);

	int strLen = lengthDistr(eng);

	string result = "";

	for (int i = 1; i <= strLen; i++)
	{
		result += asciiDistr(eng);
	}
	
	return result;
}



int main()
{
	//Measure time to complete main function
	auto start = steady_clock::now();

	//Do 100 tests with both count char methods
	for (int i = 0; i < 1000; i++)
	{
		string s = generateRandomString();
		cout << "Test " << i << endl;
		cout << "Slow Method Time: " << count_char_using_find_first_of(s, 'A') << endl;
		cout << "Fast Method Time: " << count_char_using_count(s, 'A') << endl;
		cout << endl;
	}

	auto end = steady_clock::now();
	duration<double> diff = end - start;

	cout << "Time to complete main: " << diff.count() << endl;

}
