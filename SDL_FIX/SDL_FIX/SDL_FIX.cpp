#include "stdafx.h"
#include <SDL.h>
#include <SDL_mixer.h>
#include <iostream>

Mix_Music *gBGMusic = NULL;
Mix_Chunk *gSound1 = NULL;
Mix_Chunk *gSound2 = NULL;
Mix_Chunk *gSound3 = NULL;

SDL_Window* gWindow = NULL;

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	
	//Load music
	gBGMusic = Mix_LoadMUS("bgmusic.wav");
	if (gBGMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}

	gSound1 = Mix_LoadWAV("sound1.wav");
	if (gSound1 == NULL)
	{
		printf("Failed to load high sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}

	gSound2 = Mix_LoadWAV("sound2.wav");
	if (gSound2 == NULL)
	{
		printf("Failed to load medium sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}

	gSound3 = Mix_LoadWAV("sound3.wav");
	if (gSound3 == NULL)
	{
		printf("Failed to load low sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}

	return success;
}

using namespace std;

int main(int argc, char **argv)
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		return 1;
	}
	gWindow = SDL_CreateWindow("Lab 24", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_SHOWN);
	//Initialize SDL_mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
	{
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		return 1;
	}
	
	loadMedia();

	bool quit = false;
	SDL_Event e;

	//Game loop
	while (!quit)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			//Handle key press
			else if (e.type == SDL_KEYDOWN)
			{
				switch (e.key.keysym.sym)
				{
				case SDLK_KP_1:
					cout << "Playing sound 1" << endl;
					Mix_PlayChannel(-1, gSound1, 0);
					break;
	
				case SDLK_KP_2:
					cout << "Playing sound 2" << endl;
					Mix_PlayChannel(-1, gSound2, 0);
					break;
	
				case SDLK_KP_3:
					cout << "Playing sound 3" << endl;
					Mix_PlayChannel(-1, gSound3, 0);
					break;

				case SDLK_KP_0:
					if (Mix_PlayingMusic() == 0)
					{
						cout << "Playing bg music" << endl;
						Mix_PlayMusic(gBGMusic, -1);
					}
					else
					{
						if (Mix_PausedMusic() == 1)
						{
							cout << "Playing bg music" << endl;
							Mix_ResumeMusic();
						}
						else
						{
							cout << "Pausing bg music" << endl;
							Mix_PauseMusic();
						}
					}
					break;
				}
			}
		}
	}
	return 0;
}

