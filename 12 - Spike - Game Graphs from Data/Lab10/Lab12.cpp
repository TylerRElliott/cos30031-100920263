//TYLER R ELLIOTT - 10092063
#include "stdafx.h"
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <string>
#include "State.cpp"

using namespace std;


//All inventory items inherit from this base class.
class Item {
public:
	string _name;
	int _itemID;
};

class Weapon : public Item {
public:
	int damageDealt;
	Weapon(int dmg);
};
Weapon::Weapon(int d) {
	damageDealt = d;
}

class Armor : public Item{
public:
	int resistance;
};

class Inventory {
public:
	//A map of inventory items, stored with a string (name) as key.
	map<string, Item> inv;

	void AddItem(Item i);
	void RemoveItem(string name);

	void PrintItems();
};
void Inventory::AddItem(Item i) {
	inv.insert(pair<string,Item>(i._name,i));
}
void Inventory::RemoveItem(string name) {
	inv.erase(name);
}
void Inventory::PrintItems() {
	cout << "--INVENTORY CONTENTS--" << endl;
	for (auto& kv : inv) {
		auto x = inv[kv.first];
		cout << "Item: " << x._name << " ID: " << x._itemID << endl;
	}
}

////////////////////////////////////////////////////////////
//Lab 12 Work - Implementing World Locations from TXT File//
////////////////////////////////////////////////////////////
class Location {
public:
	int _id;

	string _name;
	string _desc;

	int _edgeNorth;
	int _edgeSouth;
	int _edgeEast;
	int _edgeWest;
	int _edgeUp;
	int _edgeDown;

	Location(int id, string name, string desc, int edgeNorth, int edgeSouth, int edgeEast, int edgeWest, int edgeUp, int edgeDown);
	Location();
	void PrintLocation();
	int GetID();
	string GetName();
};
Location::Location() {

}
Location::Location(int id, string name, string desc, int edgeNorth, int edgeSouth, int edgeEast, int edgeWest, int edgeUp, int edgeDown) {
	_id = id;
	_name = name;
	_desc = desc;
	_edgeNorth = edgeNorth;
	_edgeSouth = edgeSouth;
	_edgeEast = edgeEast;
	_edgeWest = edgeWest;
	_edgeUp = edgeUp;
	_edgeDown = edgeDown;
}
void Location::PrintLocation() {
	cout << "Printing Location Info: " << endl;
	cout << "ID: " << _id << endl;
	cout << "Name: " << _name << endl;
	cout << "Description: " << _desc << endl;
	cout << "Location to North (ID): " << _edgeNorth << endl;
	cout << "Location to South (ID): " << _edgeSouth << endl;
	cout << "Location to East (ID): " << _edgeEast << endl;
	cout << "Location to West (ID): " << _edgeWest << endl;
	cout << "Location Above (ID): " << _edgeUp << endl;
	cout << "Location Below(ID): " << _edgeUp << endl;
	cout << endl;
}
int Location::GetID() {
	return _id;
}

string Location::GetName()
{
	return _name;
}

class LocationManager {
private:
	list<Location> locs;
	
public:
	Location currentLoc;
	void LoadLocs(string textFile);
	void PrintAllLocations();
	void Go();
	Location GetLocation(int id);
};

void LocationManager::LoadLocs(string textFile) {
	ifstream input(textFile);
	string line;
	string arr[9];
	int i = 0;
	while (getline(input,line)){
		if (i < 8) {
			if (line != "---") { //Ignores spaces between new locations
				arr[i] = line;
				i++;
			}
			
		}
		else {
			i = 0;
			Location newLoc(atoi(arr[0].c_str()), arr[1], arr[2], atoi(arr[3].c_str()), atoi(arr[4].c_str()), atoi(arr[5].c_str()),
				atoi(arr[6].c_str()), atoi(arr[7].c_str()), atoi(arr[8].c_str()));
			locs.push_back(newLoc);
		}
		
	}
	
}
void LocationManager::PrintAllLocations() {
	for each (Location l in locs)
	{
		l.PrintLocation();
	}
}
void LocationManager::Go(){
	cout << "You are at: " << currentLoc.GetName() << endl;
	cout << "Where do you want to go?" << endl;

	string cmd;
	cin >> cmd;
	//Some very basic commands here, for now.
	if (cmd == "up") {
		if (currentLoc._edgeUp != 0) {
			currentLoc = GetLocation(currentLoc._edgeUp);
			Go();
		}
	}
	else if (cmd == "west") {
		if (currentLoc._edgeWest != 0) {
			currentLoc = GetLocation(currentLoc._edgeWest);
			Go();
		}
	}
}
Location LocationManager::GetLocation(int id) {
	for each (Location l in locs)
	{
		if (l.GetID() == id) {
			return l;
		}
	}
}

int main()
{	
	Inventory backpack;
	LocationManager lm;
	lm.LoadLocs("Text.txt");
	lm.currentLoc = lm.GetLocation(1);
	lm.Go();
    return 0;
}

