#pragma once
#include "Health.h"
#include "Damage.h"
#include <string>

class WeaponComposite {
public:
	WeaponComposite(std::string name, int id);
	Health health;
	Damage damage;

private:
	std::string name_;
	int id_;
};
WeaponComposite::WeaponComposite(std::string name, int id) {
	name_ = name;
	id_ = id;
}