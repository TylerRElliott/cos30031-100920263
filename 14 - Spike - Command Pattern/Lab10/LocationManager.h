//Acts as the 'world' manager more than the 'location manager'

#pragma once

#include <iostream>
#include <list>
#include "Location.h"
#include <fstream>
#include <istream>
#include <string>

using namespace std;

class LocationManager {

public:
	LocationManager();
	list<Location> locs;
	Location currentLoc;
	void LoadLocs(string textFile);
	void PrintAllLocations();
	void Go();
	Location GetLocation(int id);
};
LocationManager::LocationManager() {
	//currentLoc = GetLocation(0);
}
void LocationManager::LoadLocs(string textFile) {
	ifstream input(textFile);
	string line;
	string arr[9];
	int i = 0;
	while (getline(input, line)) {
		if (i < 8) {
			if (line != "---") { //Ignores spaces between new locations
				arr[i] = line;
				i++;
			}

		}
		else {
			i = 0;
			Location newLoc(atoi(arr[0].c_str()), arr[1], arr[2], atoi(arr[3].c_str()), atoi(arr[4].c_str()), atoi(arr[5].c_str()),
				atoi(arr[6].c_str()), atoi(arr[7].c_str()), atoi(arr[8].c_str()));
			locs.push_back(newLoc);
		}

	}

}
void LocationManager::PrintAllLocations() {
	for each (Location l in locs)
	{
		l.PrintLocation();
	}
}
void LocationManager::Go() {
	cout << "You are at: " << currentLoc.GetName() << endl;
	cout << "Where do you want to go?" << endl;

	string cmd;
	cin >> cmd;
	//Some very basic commands here, for now.
	if (cmd == "up") {
		if (currentLoc._edgeUp != 0) {
			currentLoc = GetLocation(currentLoc._edgeUp);
			Go();
		}
	}
	else if (cmd == "west") {
		if (currentLoc._edgeWest != 0) {
			currentLoc = GetLocation(currentLoc._edgeWest);
			Go();
		}
	}
}
Location LocationManager::GetLocation(int id) {
	for each (Location l in locs)
	{
		if (l.GetID() == id) {
			return l;
		}
	}
}