#pragma once

#include "stdafx.h"
#include <string>
#include <iostream>

using namespace std;

class State {
public:
	State();
private:
	string _stateName;
};
State::State() {

}

//GAME STATE OBJECTS//
class MainMenu : public State {
public:
	MainMenu();
};
MainMenu::MainMenu() {
	cout << "--MAIN MENU--" << endl;
}

class About : public State {
public:
	About();
};
About::About() {
	cout << "--ABOUT--" << endl;
}

class Help : public State {
public:
	Help();
};
Help::Help() {
	cout << "--HELP--" << endl;
}

class SelectAdventure : public State {
public:
	SelectAdventure();
};
SelectAdventure::SelectAdventure() {
	cout << "--SELECT ADVENTURE--" << endl;
}

class Gameplay : public State {
public:
	Gameplay();
};
Gameplay::Gameplay() {
	cout << "--GAMEPLAY--" << endl;
}

class NewHighScore : public State {
public:
	NewHighScore();
};
NewHighScore::NewHighScore() {
	cout << "--NEW HIGH SCORE--" << endl;
}

class ViewHallOfFame : public State {
public:
	ViewHallOfFame();
};
ViewHallOfFame::ViewHallOfFame() {
	cout << "--VIEW HALL OF FAME--" << endl;
}

//THE GAME STATE MANAGER//

class StateManager {
public:
	bool gameOver = false;
	State curState;
	StateManager();
	void SwitchState(State s);
};
StateManager::StateManager() {
	curState = MainMenu();
}
void StateManager::SwitchState(State s) {
	curState = s;
}



