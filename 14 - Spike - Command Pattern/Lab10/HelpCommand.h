#pragma once
#include "Command.h"
#include "State.h"
#include "CommandManager.h"
class HelpCommand : public Command {
public:
	void execute() {
		cout << "--Available Commands--" << endl;
		cout << "1. HELP" << endl;
		cout << "2. INVENTORY" << endl;
		cout << "3. LOOK (+ N/S/E/W/UP/DOWN)" << endl;
		cout << "4. LOOKAT (+item name)" << endl;
		cout << "5. ALIAS [old command name] [new command name]" << endl;
		cout << "6. DEBUGTREE" << endl;
		cout << endl;
	}
private:
	StateManager* _sm;
};