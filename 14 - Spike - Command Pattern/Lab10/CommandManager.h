#pragma once
#include "Command.h"
#include "HelpCommand.h"
#include "InventoryCommand.h"
#include "LookCommand.h"
#include "AliasCommand.h"
#include "DebugTreeCommand.h"

class CommandManager {
public:
	CommandManager(StateManager* sm,  LocationManager* lm, Inventory* inv);
	void handleInput();

private:
	map<string, Command*>commands;

	Command* currentCmd_;

	StateManager* stateMgr_;
	LocationManager* locationMgr_;
	Inventory* inventory_;
};
CommandManager::CommandManager(StateManager* sm, LocationManager* lm, Inventory* inv) {
	stateMgr_ = sm;
	inventory_ = inv;
	locationMgr_ = lm;

	//Registers all commands.
	commands["HelpCommand"] = new HelpCommand();
	commands["InventoryCommand"] = new InventoryCommand(inventory_);
	commands["LookCommand"] = new LookCommand(locationMgr_);
	commands["LookAtCommand"] = new LookAtCommand(inventory_,"");
	commands["AliasCommand"] = new AliasCommand();
	commands["DebugTreeCommand"] = new DebugTreeCommand(locationMgr_);
}

void CommandManager::handleInput() {
	string input = "";

	cout << "What would you like to do?" << endl;
	cin >> input;

	//Convert to uppercase
	for (auto &c : input) c = toupper(c);
	
	//Get the verb (to determine action)
	string v = input.substr(0, input.find(" "));

	//C++ doesn't allow switch methods for strings,
	//so we do this the painful way.
	if (v == "HELP") {
		currentCmd_ = commands["HelpCommand"];
	}
	else if (v == "INVENTORY") {
		currentCmd_ = commands["InventoryCommand"];
	}
	else if (v == "LOOK") {
		currentCmd_ = commands["LookCommand"];
	}
	else if (v == "LOOKAT") {
		currentCmd_ = new LookAtCommand(inventory_, "Axe");
	}
	else if (v == "ALIAS") {
		currentCmd_ = commands["AliasCommand"];
	}
	else if (v == "DEBUGTREE") {
		currentCmd_ = commands["DebugTreeCommand"];
	}
	else {
		cout << "I don't understand " << v << endl;
		return;
	}
	currentCmd_->execute();
}
