#pragma once
#include "Command.h"
#include <iostream>

class AliasCommand : public Command {
public:
	AliasCommand() {}
	void execute() {
		cout << "EXECUTING AliasCommand!";
	}
private:

};
