#pragma once
#include "Command.h"
#include <iostream>

class InventoryCommand : public Command {
public:
	InventoryCommand();
	InventoryCommand(Inventory* inv);
	void execute() {
		_inv->PrintItems();
	}
private:
	Inventory* _inv;
};
InventoryCommand::InventoryCommand() {
	_inv = NULL;
}
InventoryCommand::InventoryCommand(Inventory* inv) {
	_inv = inv;
}
