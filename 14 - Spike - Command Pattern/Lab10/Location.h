#pragma once
#include <iostream>

using namespace std;

////////////////////////////////////////////////////////////
//Lab 12 Work - Implementing World Locations from TXT File//
////////////////////////////////////////////////////////////
class Location {
public:
	int _id;

	string _name;
	string _desc;

	list<Item>items;

	int _edgeNorth;
	int _edgeSouth;
	int _edgeEast;
	int _edgeWest;
	int _edgeUp;
	int _edgeDown;

	Location(int id, string name, string desc, int edgeNorth, int edgeSouth, int edgeEast, int edgeWest, int edgeUp, int edgeDown);
	Location();
	void PrintLocation();
	int GetID();
	string GetName();
};
Location::Location() {

}
Location::Location(int id, string name, string desc, int edgeNorth, int edgeSouth, int edgeEast, int edgeWest, int edgeUp, int edgeDown) {
	_id = id;
	_name = name;
	_desc = desc;
	_edgeNorth = edgeNorth;
	_edgeSouth = edgeSouth;
	_edgeEast = edgeEast;
	_edgeWest = edgeWest;
	_edgeUp = edgeUp;
	_edgeDown = edgeDown;
}
void Location::PrintLocation() {
	cout << "ID: " << _id << endl;
	cout << "Name: " << _name << endl;
	cout << "Description: " << _desc << endl;
	cout << "Location to North (ID): " << _edgeNorth << endl;
	cout << "Location to South (ID): " << _edgeSouth << endl;
	cout << "Location to East (ID): " << _edgeEast << endl;
	cout << "Location to West (ID): " << _edgeWest << endl;
	cout << "Location Above (ID): " << _edgeUp << endl;
	cout << "Location Below(ID): " << _edgeUp << endl;
	cout << endl;
}
int Location::GetID() {
	return _id;
}

string Location::GetName()
{
	return _name;
}