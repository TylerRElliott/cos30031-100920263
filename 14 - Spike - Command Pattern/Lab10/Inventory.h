#pragma once

#include "stdafx.h"
#include <iostream>
#include "Command.h"
#include "Item.h"

using namespace std;

class Inventory {
public:
	//A map of inventory items, stored with a string (name) as key.
	map<string, Item> inv;

	Inventory();

	void AddItem(Item i);
	void RemoveItem(string name);
	void LookAtItem(string name);
	void PrintItems();
	
	void LookAt() {
		PrintItems();
	}
};
Inventory::Inventory() {

}
void Inventory::LookAtItem(string name) {
	Item temp = inv["Axe"];
	cout << "Looking at " << temp._name << endl;
}
void Inventory::AddItem(Item i) {
	inv.insert(pair<string, Item>(i._name, i));
}
void Inventory::RemoveItem(string name) {
	inv.erase(name);
}
void Inventory::PrintItems() {
	cout << "--INVENTORY CONTENTS--" << endl;
	for (auto& kv : inv) {
		auto x = inv[kv.first];
		cout << "Item: " << x._name << " ID: " << x._itemID << endl;
	}
}


