#pragma once
#include "Command.h"
#include <iostream>
#include "LocationManager.h"

class LookCommand : public Command {
public:
	LookCommand();
	LookCommand(LocationManager* lm);
	void execute() {
		lm_->currentLoc.PrintLocation();
	}
private:
	LocationManager* lm_;
};
LookCommand::LookCommand() {
	lm_ = NULL;
}
LookCommand::LookCommand(LocationManager* lm) {
	lm_ = lm;
}
