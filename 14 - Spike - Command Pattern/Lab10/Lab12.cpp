//LAB 14 - TYLER R ELLIOTT - 10092063

#include "stdafx.h"
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <string>
#include "State.h"
#include "LookAtCommand.h"
#include "CommandManager.h"
#include "LocationManager.h"
#include "WeaponComposite.h"

using namespace std;

int main()
{	
	//Load the 'game'
	LocationManager* lm = new LocationManager();
	lm->LoadLocs("Text.txt");

	//Setup an inventory and some items
	Inventory* inv = new Inventory();
	
	Weapon axe("Axe", 1, 150);
	Armor helmet("Helmet", 2, 25);

	WeaponComposite sword("Greatsword",3);
	sword.damage.dmgAmount = 10;
	sword.health.healthAmt = 100;

	inv->AddItem(helmet);
	inv->AddItem(axe);
	
	StateManager *sm = new StateManager();
	CommandManager cmdMgr(sm,lm,inv);

	//The game loop
	while (true) {
		cmdMgr.handleInput();
	}

    return 0;
}

