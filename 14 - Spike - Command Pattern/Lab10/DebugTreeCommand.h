#pragma once
#include "Command.h"
#include "LocationManager.h"
#include <iostream>

class DebugTreeCommand : public Command {
public:
	DebugTreeCommand();
	DebugTreeCommand(LocationManager* lm);
	void execute() {
		cout << "Running Debug Tree" << endl;
		lm_->PrintAllLocations();
	}
private:
	LocationManager* lm_;
};
DebugTreeCommand::DebugTreeCommand() {
	lm_ = NULL;
}
DebugTreeCommand::DebugTreeCommand(LocationManager* lm) {
	lm_ = lm;
}
