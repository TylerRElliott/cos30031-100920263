#pragma once


//All inventory items inherit from this base class.
class Item {
public:
	Item();
	Item(string name, int id);
	string _name;
	int _itemID;
	void LookAt();
};
//Default Constructor
Item::Item() {
	_name = "";
	_itemID = NULL;
}
Item::Item(string name, int id) {
	_name = name;
	_itemID = id;
}
void Item::LookAt() {
	cout << "Looking at " << _name << endl;
}

class Weapon : public Item {
public:
	Weapon(string name, int id, int damage);
	int _damageDealt;
};
Weapon::Weapon(string name, int id, int damage):Item(name,id){
	_damageDealt = damage;
}

class Armor : public Item {
public:
	Armor(string name, int id, int res);
	int _resistance;
};
Armor::Armor(string name, int id, int res) :Item(name, id) {
	_resistance = res;
}
