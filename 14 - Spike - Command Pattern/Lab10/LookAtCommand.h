#pragma once
#include "Command.h"
#include "Inventory.h"
#include "Item.h"

class LookAtCommand : public Command {
public:
	LookAtCommand(Inventory* inv, string name);
	void execute() {
		cout << "Looking at.." << itemName << endl;
		mInv->LookAtItem(itemName);
	}
private:
	string itemName;
	Inventory *mInv;
};
LookAtCommand::LookAtCommand(Inventory* inv, string name){
	itemName = name;
}