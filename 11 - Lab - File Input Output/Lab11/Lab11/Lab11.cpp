// Lab11.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <nlohmann\json.hpp>
#include <iostream>
#include <fstream>

using json = nlohmann::json;
using namespace std;

class PartA {
public:
	char c;
	int i;
	float f;
	void PrintValues();
	void WriteToBinFile();
	void ReadFromBinFile();
};
void PartA::PrintValues() {
	cout << "Part A Values: " << endl;
	cout << "Char: " << c << endl;
	cout << "Int: " << i << endl;
	cout << "Float: " << f << endl;
}
void PartA::WriteToBinFile() {
	//Open file
	c = 'B';
	i = 2;
	f = 2.2;
	ofstream file("parta.bin", ios::binary);
	file.put(c);
	file.write(reinterpret_cast<const char*>(&i), sizeof(int));
	file.write(reinterpret_cast<const char*>(&f), sizeof(float));
	file.close();
}
void PartA::ReadFromBinFile() {
	ifstream is("parta.bin",ios::binary);
	char _c;
	is.seekg(0);
	is.read((char*)&_c, 1);
	c = _c;
	int _i;
	is.seekg(1);
	is.read((char*)&_i, 4);
	i = _i;
	float _f;
	is.seekg(5);
	is.read((char*)&_f, 4);
	f = _f;
	is.close();
}

class PartB {
public:
	void ReadFromFile(string path);
};
void PartB::ReadFromFile(string path) {
	ifstream file(path);
	string line;
	string fileContents;
	while (getline(file,line,'\n')) {
		if (line != "" && line[0] != '#') {
			fileContents += line + "\n";
		}
	}
	std::cout << "File Contents: "<<endl << fileContents << endl;

}

class PartC {
public:
	void ReadFromFile(string path);
};
void PartC::ReadFromFile(string path){
	ifstream ifs(path);
	json j= json::parse(ifs);
	
	cout << j << endl;
}

int main()
{
	//Part A - Binary File I/O
	cout << "PART A: " << endl;
	PartA pa;
	pa.c = 'A';
	pa.i = 1;
	pa.f = 1.1f;
	pa.PrintValues();
	pa.WriteToBinFile();
	pa.ReadFromBinFile();
	pa.PrintValues();
	cout << endl;
	
	//Part B - Text File I/O
	cout << endl;
	cout << "PART B: " << endl;
	PartB pb;
	pb.ReadFromFile("partb.txt");
	cout << endl;

	//Part C - JSON I/O
	cout << "PART C: " << endl;
	PartC pc;
	pc.ReadFromFile("partc.json");
	cout << endl;

    return 0;
}

