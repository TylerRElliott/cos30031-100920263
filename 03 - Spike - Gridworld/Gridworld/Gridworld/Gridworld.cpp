#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <list>

using namespace std;

class Gridworld {
	
public:
	Gridworld() {
		cout << "Welcome to Gridworld!" << endl;
	}
	
	//Game world - grid //0 represents an empty space //1 represents a wall
	//2 represents death//3 represents gold
	int grid[8][8] =
	{
		{ 1,1,1,1,1,1,1,1 },
		{ 1,3,0,2,1,2,0,1 },
		{ 1,0,0,0,1,0,0,1 },
		{ 1,1,1,0,1,0,2,1 },
		{ 1,0,0,0,1,0,0,1 },
		{ 1,0,1,1,1,1,0,1 },
		{ 1,0,0,0,0,0,0,1 },
		{ 1,1,0,1,1,1,1,1 }
	};
	//Initialize start location
	int posRow = 1;
	int posCol = 1;

	//True when either the player is killed or the treasure is found
	bool endGame = false;

	//Stores cin in input var and converts to lowercase
	string getUserInput() {
		cout << "Enter a direction: ";
		string input;
		getline(cin, input);
		transform(input.begin(), input.end(), input.begin(), ::tolower);
		return input;
	}

	void checkForDeathSquares() {
		if (posRow == 1 && posCol == 3 || posRow == 1 && posCol == 5 || posRow == 3 && posCol == 6) {
			cout << "You have been killed to death. Game over." << endl;
			endGame = true;
		}
	}

	void checkForTreasureSquares() {
		if (posRow == 1 && posCol == 1) {
			cout << "You have found the treasure. You win!" << endl;
			endGame = true;
		}
	}

	void Move() {
		//List stores 1-4 available movement directions
		list<string> dirs = list<string>();

		//Check North
		if (posRow - 1 > 0 && grid[posRow-1][posCol] != 1) {
			dirs.push_front("N");
		}
		//Check South
		if (posRow + 1 < 8 && grid[posRow + 1][posCol] != 1) {
			dirs.push_front("S");
		}
		//Check East
		if (posCol + 1 < 8 && grid[posRow][posCol + 1] != 1) {
			dirs.push_front("E");
		}
		//Check West
		if (posCol - 1 > 0 && grid[posRow][posCol-1] != 1) {
			dirs.push_front("W");
		}
		//Loop through list and output available directions
		cout << "You can move ";
		for each (string s in dirs)
		{
			cout << s << ",";
		}
		cout << endl << endl;

		checkForDeathSquares();
		checkForTreasureSquares();
		
		//Move North
		string input = getUserInput();
		
		if (input == "n") {
			if (posRow - 1 > 0 && grid[posRow - 1][posCol] != 1) {
				cout << "You moved north." << endl;
				posRow--;
			}
			else {
				cout << "Can't move that way!" << endl;
			}
		}
		//Move South
		else if (input == "s") {
			if (posRow + 1 < 8 && grid[posRow + 1][posCol] != 1) {
				cout << "You moved south." << endl;
				posRow++;
			}
			else {
				cout << "Can't move that way!" << endl;
			}
		}
		//Move East
		else if (input == "e") {
			if (posCol + 1 < 8 && grid[posRow][posCol + 1] != 1) {
				cout << "You moved east." << endl;
				posCol++;
			}
			else {
				cout << "Can't move that way!" << endl;
			}
		}
		//Move West
		else if (input == "w") {
			if (posCol - 1 > 0 && grid[posRow][posCol - 1] != 1) {
				cout << "You moved west." << endl;
				posCol--;
			}
			else {
				cout << "Can't move that way!" << endl;
			}
		}
		else {
			cout << "Please enter N,S,E or W!" << endl;
		}
	}

};

int main()
{
	Gridworld gw;
	
	while (!gw.endGame) {
		gw.Move();
	}
		
	return 0;
}

